cd portfolio
Explained: Change directory to a folder named "portfolio".

git switch master
Explained: Switch to the branch named "master" in a Git repository.

mkdir -p week4/framework
Explained: Create a folder structure where "framework" is a subdirectory of "week4".

cd week4/framework
Explained: Change directory to the "framework" folder inside "week4".

git branch framework
Explained: Create a new branch named "framework" in the Git repository.

git switch framework
Explained: Switch to the branch named "framework" in the Git repository.

nano Makefile
<tab>mk(NAME) ;\
       <tab(NAME) && \
       <tab>mkdir bin doc src test lib config ;\
       <tab>echo “*” > bin/.gitignore ;\
       <tab>echo “*” > lib/.gitignore
Explained: Creates folders named bin, doc, src, test, lib, and config. Creates a .gitignore file inside the "bin" folder and adds "*" (wildcard) to it. Creates a .gitignore file inside the "lib" folder and adds "*" (wildcard) to it.

cat -vTE Makefile
Explained: Used to display the contents of the file "Makefile" while showing special characters, line endings, and tabs in a visible format.

make feature NAME=test_output
Explained: Executes a "make" command with the goal "feature" and an argument "NAME=test_output," likely triggering a build or action related to creating a feature.

ls -al test_output
Explained: Lists all files and directories in the "test_output" directory, showing detailed information.

git add Makefile
Explained: Stages the "Makefile" changes for commit in the Git version control system.

git commit -m "Setting up Makefile to create feature folders"
Explained: Commits the staged changes with a message describing the changes made to the "Makefile."

git push
Explained: Uploads committed changes to the remote repository, synchronizing the local changes with the remote repository.

cd test_output; cd src
Explained: navigates into the "src" directory within the "test_output" directory, moving through nested directories.

test_outputs.c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define COM_SIZ 60
#define ARG_SIZ 1024
#define RES_SIZ 1024
/** \fn int main(int argc, char *argv[])
int main(int argc, char *argv[]) {
FILE *fp; /**< fp is a pointer used to give access to the file
descriptor of the pipe */
  FILE *tests;
  char command[COM_SIZ];
  char arguments[ARG_SIZ];
  char expected[RES_SIZ];
  char actual[RES_SIZ];
  char command_line[COM_SIZ + ARG_SIZ];
  tests=fopen(argv[1], "rt");
  if (tests==NULL) {
printf("Error opening file %s\n",argv[1]);
return 1; }
while (fgets(command, COM_SIZ, tests) != NULL) {
    fgets(arguments, ARG_SIZ, tests);
    fgets(expected, RES_SIZ, tests);
strtok(command, "\n");
snprintf(command_line, sizeof command_line, "%s %s", command, arguments);
   CS1PC20, Exercise – Week 4 5/8
  * the "popen" command opens a special type of 'file' called a 'pipe' */
  fp= popen(command_line , "r");
  if (fp == NULL){
    printf("Failed to run command\n");
exit(1); }
            char message[RES_SIZ + RES_SIZ + 21];
while(fgets(actual, sizeof(actual), fp)
snprintf(message, sizeof message, "%s %s %s %s",
"Expected ", expected, " and got ", actual); printf("%s",message);
if(!strcmp(actual, expected)) {
                printf("OK\n");
              }
              else {
                printf("FAILED\n");
} }
          }
          fclose(tests);
          }
          return 0;
}
Explained: The C code in "test_outputs.c" reads test definitions from a file. For each test case (command, inputs, expected output), it executes the command with provided inputs, captures the output, and compares it to the expected result. It prints "OK" if the actual output matches the expected output and "FAILED" otherwise. This process ensures checking expected program outputs against actual outputs.

gcc -Wall -pedantic test_outputs.c -o test_outputs
Explained: The command compiles the C file "test_outputs.c" into an executable named "test_outputs" while enforcing strict compiler warnings and additional pedantic checks.

./test_outputs file_does_not_exist
Explained: Runs the executable file named "test_outputs" and provides "file_does_not_exist" as an argument to it.

./test_output
Explained: Runs the executable file named "test_outputs" without providing any additional arguments.

op_test
  wc -l
       test_outputs.c
       108 test_outputs.c
       wc -l
       test_outputs.c
       82 test_outputs.c
       wc -l
       test_outputs.c
       108 Test_outputs.c
Explained: Creates a file named "op_test" that contains commands to count lines in the file "test_outputs.c" and its variations in filename case, without any newline character at the end of the file.

./test_outputsop_test
Explained:Executes the program "test_outputs" with the argument "op_test".

gitaddtest_outputs.c
Explained: Stages changes made to the file named "test_outputs.c" for commit in Git.

gitaddop_test
Explained: Stages changes made to the file named "op_test" for commit in Git.

git commit -m "test framework and sample test suite"
Explained: Commits the staged changes with a message describing them as "test framework and sample test suite" in Git.

gitpush
Explained: Uploads committed changes to the remote repository in Git, synchronizing the local changes with the remote repository.

history > my_work_log.md
Explained: Saves the list of previously executed commands into a file named "my_work_log.md".
